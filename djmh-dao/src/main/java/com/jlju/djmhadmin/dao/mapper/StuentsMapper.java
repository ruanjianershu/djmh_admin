package com.jlju.djmhadmin.dao.mapper;


import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.Stuents;

public interface StuentsMapper extends GenericMapper<Stuents> {

}
