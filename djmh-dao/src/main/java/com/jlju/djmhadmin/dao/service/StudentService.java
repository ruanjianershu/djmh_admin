/**  
 * All rights Reserved, Designed By Liting.
 * @author: liyang[li_yang@Liting.com]
 * @date: 2017-10-31 16:25:47  
 * @Copyright ©2017 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 */
package com.jlju.djmhadmin.dao.service;


import com.jlju.djmhadmin.dao.common.GenericService;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.User;

public interface StudentService extends GenericService<Student> {

    Student getByCard(String cardId);

    Student getByAcNo(Student student);
}