package com.jlju.djmhadmin.dao.service;

import com.jlju.djmhadmin.dao.entity.User;

import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: service
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-20 10:03
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
public interface TestService {
    List<User> find();
}