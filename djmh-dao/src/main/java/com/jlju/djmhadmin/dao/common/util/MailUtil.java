package com.jlju.djmhadmin.dao.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 邮件发送
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-24 13:30
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
public class MailUtil {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    //发送邮件方法
    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail, String msg) throws Exception {
        // 1. 创建一封邮件
        MimeMessage message = new MimeMessage(session);

        // 2. From: 发件人
        message.setFrom(new InternetAddress(sendMail, "PJT", "UTF-8"));

        // 3. To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "HELLO", "UTF-8"));

        // 4. Subject: 邮件主题
        message.setSubject("吉林建筑大学电气与计算机学院门户通知 ： ", "UTF-8");

        // 5. Content: 邮件正文（可以使用html标签
        message.setContent("提醒内容 ： " + msg, "text/html;charset=UTF-8");

        // 6. 设置发件时间
        message.setSentDate(new Date());

        // 7. 保存设置
        message.saveChanges();

        return message;
    }

    public void postMail(String post, String conText) {
        logger.info("****发送邮件service层****");
        try {
            //创建邮件
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
            props.setProperty("mail.smtp.host", EmailUtil.myEmailSMTPHost);   // 发件人的邮箱的 SMTP 服务器地址
            props.setProperty("mail.smtp.auth", "false");            // 需要请求认证
            // 用于连接邮件服务器的参数配置（发送邮件时才需要用到）
            Session session = Session.getInstance(props);        // 根据参数配置，创建会话对象（为了发送邮件准备的）
            MimeMessage message = MailUtil.createMimeMessage(session, EmailUtil.myEmailAccount, post, conText);    // 创建邮件对象
            // 4. 根据 Session 获取邮件传输对象
            Transport transport = session.getTransport();
            transport.connect(EmailUtil.myEmailAccount, EmailUtil.myEmailPassword);//到这报错
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            logger.info("信息发送成功");
        } catch (Exception e) {
            logger.error("邮件发送失败 ： " + e);
            e.printStackTrace();
        }
    }
}