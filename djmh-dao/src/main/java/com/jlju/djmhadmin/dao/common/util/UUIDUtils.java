package com.jlju.djmhadmin.dao.common.util;

import java.util.UUID;

/**
 * All rights Reserved, Designed By Suixingpay.
 *
 * @description: uuid 工具类
 * @author: LiTing[li_ting1@suixingpay.com]
 * @create: 2018-04-26 21:22
 * @Copyright ©2018 Suixingpay. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
public class UUIDUtils {

    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}