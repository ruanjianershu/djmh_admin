package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.entity.Files;
import com.jlju.djmhadmin.dao.mapper.FilesMapper;
import com.jlju.djmhadmin.dao.service.FilesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * All rights Reserved, Designed By Suixingpay.
 *
 * @description: 文件操作
 * @author: LiTing[li_ting1@suixingpay.com]
 * @create: 2018-05-03 11:35
 * @Copyright ©2018 Suixingpay. All rights reserved.
 * 注意：本内容仅限于随行付支付有限公司内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class FilesServiceImpl implements FilesService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FilesMapper filesMapper;


    @Override
    public void add(Files var1) {
        filesMapper.add(var1);
    }

    @Override
    public int updateById(Files var1) {
        return 0;
    }

    @Override
    public void deleteById(Files var1) {
        filesMapper.deleteById(var1);
    }

    @Override
    public Files findOneById(String pk) {
        return null;
    }

    @Override
    public List<Files> findAll(Files pk) {
        return filesMapper.findAll(pk);
    }
}