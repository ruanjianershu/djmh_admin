package com.jlju.djmhadmin.dao.mapper;


import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.Teacher;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 教师相关信息mapper
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:41
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
public interface TeacherMapper extends GenericMapper<Teacher>{

    Teacher getByCard(String cardId);

    Teacher getByAcNo(Teacher teacher);
}