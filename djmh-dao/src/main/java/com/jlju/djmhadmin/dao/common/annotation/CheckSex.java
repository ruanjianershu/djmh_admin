package com.jlju.djmhadmin.dao.common.annotation;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CheckSex {

     // @LOMBOK 注解 省去写get set 的时间
     //  @Data
    //生成构造器  @AllArgsConstructor
    // @Builder  链式调用  Employee e = new Employee();
    //  会被替代为  Employee e2 = Employee.builder().id(2).name("aa");

    int max() default 0;
    int min() default 0;
    String message() default "";

}
