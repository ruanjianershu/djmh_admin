package com.jlju.djmhadmin.dao.service;


import com.jlju.djmhadmin.dao.common.GenericService;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.Stuents;

public interface StuentsService extends GenericService<Stuents> {
    void postMail(String post, String conText);
}
