package com.jlju.djmhadmin.dao.common;

import java.util.List;

public interface GenericService<T> {

    void add(T var1);

    int updateById(T var1);

    void deleteById(T var1);

    T findOneById(String pk);

    List<T> findAll(T pk);

}
