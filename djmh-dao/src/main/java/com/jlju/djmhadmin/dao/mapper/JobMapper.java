package com.jlju.djmhadmin.dao.mapper;


import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.Job;

public interface JobMapper extends GenericMapper<Job> {

    Job getById(Job job);
}
