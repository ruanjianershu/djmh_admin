package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.entity.Job;
import com.jlju.djmhadmin.dao.entity.Notice;
import com.jlju.djmhadmin.dao.mapper.JobMapper;
import com.jlju.djmhadmin.dao.mapper.NoticeMapper;
import com.jlju.djmhadmin.dao.service.JobService;
import com.jlju.djmhadmin.dao.service.NoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 求职信息管理
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 20:23
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
@Slf4j
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public void add(Notice var1) {
        this.noticeMapper.add(var1);
    }

    @Override
    public int updateById(Notice var1) {
        return this.noticeMapper.updateById(var1);
    }

    @Override
    public void deleteById(Notice var1) {
        this.noticeMapper.deleteById(var1);
    }

    @Override
    public Notice findOneById(String pk) {
        return null;
    }

    @Override
    public List<Notice> findAll(Notice pk) {
        return this.noticeMapper.findAll(pk);
    }

    @Override
    public List<Notice> findAdmin(Notice pk) {
        return this.noticeMapper.findAdmin(pk);
    }
}