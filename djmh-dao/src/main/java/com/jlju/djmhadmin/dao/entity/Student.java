package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 学生信息
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:18
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    public Student(String uuid) {
        this.uuid = uuid;
    }

    public Student(String name, String acNo) {
        this.name = name;
        this.acNo = acNo;
    }

    private String uuid;
    private String name;
    private String sex;
    private String className;
    //学号
    private String acNo;
    //家长电话
    private String parPhone;
    private String phone;
    private String QQ;
    //身份证号
    private String cardId;
    private String jobCity;
    private String email;
    private String national;
    /**学籍状态 0 ： 待报道 1 ： 在读  2： 休学 3 ： 退学  4： 毕业*/
    private String status;
    private String photo;
    private String createDate;
    //级别（2015）
    private String classLevel;


}