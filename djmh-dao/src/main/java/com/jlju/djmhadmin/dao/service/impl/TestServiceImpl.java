package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.entity.User;
import com.jlju.djmhadmin.dao.mapper.UserMapper;
import com.jlju.djmhadmin.dao.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: impl
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-20 10:06
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> find() {
        return userMapper.findAll();
    }
}