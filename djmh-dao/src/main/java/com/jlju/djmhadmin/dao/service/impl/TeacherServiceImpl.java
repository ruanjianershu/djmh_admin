package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.common.util.EmailUtil;
import com.jlju.djmhadmin.dao.common.util.MailUtil;
import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.Teacher;
import com.jlju.djmhadmin.dao.mapper.TeacherMapper;
import com.jlju.djmhadmin.dao.service.TeacherService;
import com.jlju.djmhadmin.dao.service.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 教师档案相关操作类
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:40
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class TeacherServiceImpl implements TeacherService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Resource
    private TeacherMapper teacherMapper;

    @Override
    public void add(Teacher var1) {
        logger.info("****教师档案添加service层****");
        var1.setUuid(UUIDUtils.getUuid());
        var1.setStatus("0");
        var1.setCreateDate(new Date().toString());
        this.teacherMapper.add(var1);
    }

    @Override
    public int updateById(Teacher var1) {
        logger.info("****教师档案更新service层****");
        return this.teacherMapper.updateById(var1);
    }

    @Override
    public void deleteById(Teacher var1) {
        logger.info("****教师档案注销service层****");
        this.teacherMapper.deleteById(var1);
    }

    @Override
    public Teacher findOneById(String pk) {
        logger.info("****教师档案查看详细service层****");
        return this.teacherMapper.findOneById(pk);
    }

    @Override
    public List<Teacher> findAll(Teacher pk) {
        logger.info("****教师档案查询service层****");
        return teacherMapper.findAll(pk);
    }

    @Override
    public Teacher getByCard(String cardId) {
        logger.info("****根据身份证号查询教师档案service层****");
        return teacherMapper.getByCard(cardId);
    }

    @Override
    public Teacher getByAcNo(Teacher teacher) {
        return teacherMapper.getByAcNo(teacher);
    }
}