package com.jlju.djmhadmin.dao.mapper;

import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author liting
 * @version [版本号, YYYY-MM-DD]
 * @date 2017/11/8
 * @see [相关类/方法]
 */
public interface UserMapper extends GenericMapper<User> {

    User login(@Param("acNo") String account, @Param("password") String password);

    User findByAccount(String account);
    //密码后台默认更改
    void password(User user);

    List<User> findAll();
}
