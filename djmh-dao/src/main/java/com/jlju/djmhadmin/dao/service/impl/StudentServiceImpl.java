package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.common.util.EmailUtil;
import com.jlju.djmhadmin.dao.common.util.MailUtil;
import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.User;
import com.jlju.djmhadmin.dao.mapper.StudentMapper;
import com.jlju.djmhadmin.dao.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 学生信息相关操作类
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:40
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class StudentServiceImpl implements StudentService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private StudentMapper studentMapper;

    @Override
    public void add(Student var1) {
        logger.info("****学生信息添加service层****");
        var1.setUuid(UUIDUtils.getUuid());
        var1.setStatus("0");
        var1.setCreateDate(new Date().toString());
        this.studentMapper.add(var1);
    }

    @Override
    public int updateById(Student var1) {
        logger.info("****学生信息更新service层****");
        return this.studentMapper.updateById(var1);
    }

    @Override
    public void deleteById(Student var1) {
        logger.info("****学生信息注销service层****");
        this.studentMapper.deleteById(var1);
    }

    @Override
    public Student findOneById(String pk) {
        logger.info("****学生信息查看详细service层****");
        return this.studentMapper.findOneById(pk);
    }

    @Override
    public List<Student> findAll(Student pk) {
        logger.info("****学生信息查询service层****");
        return studentMapper.findAll(pk);
    }

    @Override
    public Student getByCard(String cardId) {
        logger.info("****根据身份证号查询学生信息service层****");
        return studentMapper.getByCard(cardId);
    }

    @Override
    public Student getByAcNo(Student student) {
        logger.info("***请假验证学生信息***");
        return this.studentMapper.getByAcNo(student);
    }

    public static void main(String[] args) {
        MailUtil mailUtil = new MailUtil();
        mailUtil.postMail("liting08011@163.com", "aaaaaaa");
    }
}