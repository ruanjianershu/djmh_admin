package com.jlju.djmhadmin.dao.mapper;


import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.Notice;

import java.util.List;

public interface NoticeMapper extends GenericMapper<Notice> {
    List<Notice> findAdmin(Notice pk);
}
