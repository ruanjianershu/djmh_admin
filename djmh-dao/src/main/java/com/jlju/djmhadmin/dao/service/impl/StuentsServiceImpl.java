package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Stuents;
import com.jlju.djmhadmin.dao.mapper.StuentsMapper;
import com.jlju.djmhadmin.dao.service.StuentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 请假信息维护
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 20:24
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class StuentsServiceImpl implements StuentsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private StuentsMapper stuentsMapper;

    @Override
    public void add(Stuents var1) {
        logger.info("***请假申请service层***");
        var1.setUuid(UUIDUtils.getUuid());
        var1.setStartDate(new Date().toString());
        var1.setStatus("0");
        stuentsMapper.add(var1);
    }

    @Override
    public int updateById(Stuents var1) {
        logger.info("***更新请假信息service层***");
        return this.stuentsMapper.updateById(var1);
    }

    @Override
    public void deleteById(Stuents var1) {
        logger.info("***销假service层***");
        this.stuentsMapper.deleteById(var1);
    }

    @Override
    public Stuents findOneById(String pk) {
        logger.info("***查询请假详细信息service层***");
        return this.stuentsMapper.findOneById(pk);
    }

    @Override
    public List<Stuents> findAll(Stuents pk) {
        logger.info("***查询请假信息service层***");
        return stuentsMapper.findAll(pk);
    }

    @Override
    public void postMail(String post, String conText) {

    }
}