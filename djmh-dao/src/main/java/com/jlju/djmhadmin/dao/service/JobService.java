package com.jlju.djmhadmin.dao.service;


import com.jlju.djmhadmin.dao.common.GenericService;
import com.jlju.djmhadmin.dao.entity.Job;

public interface JobService extends GenericService<Job>{
    Job getById(Job job);
}
