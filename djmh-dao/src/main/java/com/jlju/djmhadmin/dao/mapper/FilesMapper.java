package com.jlju.djmhadmin.dao.mapper;


import com.jlju.djmhadmin.dao.common.GenericMapper;
import com.jlju.djmhadmin.dao.entity.Files;
import com.jlju.djmhadmin.dao.entity.Job;

public interface FilesMapper extends GenericMapper<Files> {

}
