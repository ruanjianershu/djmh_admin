package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 学生信息
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:18
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    public Teacher(String uuid) {
        this.uuid = uuid;
    }

    private String uuid;
    private String name;
    private String sex;
    //职称
    private String title;
    //工号
    private String acNo;
    //电话
    private String phone;
    private String QQ;
    //身份证号
    private String cardId;
    //备注
    private String memo;
    private String email;
    //研究方向
    private String direction;
    /** 0 ： 正常 1 ： 失效 */
    private String status;
    private String photo;
    //所属教研室
    private String profession;
    private String createDate;

}