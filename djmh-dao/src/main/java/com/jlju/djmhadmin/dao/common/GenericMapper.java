package com.jlju.djmhadmin.dao.common;





import java.util.List;


/**
* @Description: 通用mapper
* @Param:
* @return:
* @Author: LiTing [liting08011@163.com]
* @Date: 2018/4/19
* @Time: 下午3:4
*/
public interface GenericMapper<T> {

    /**
     *
     * 添加数据
     * @param entity
     * @return
     */
    Integer add(T entity);

    /**
     *
     * 修改数据
     * @param entity
     * @return
     */
    Integer updateById(T entity);

    /**
     *
     * 删除数据
     * @param entity
     * @return
     */
    Integer deleteById(T entity);

    /**
     *
     * 查询所有数据功能
     * @return
     */
    List<T> findAll(T pk);

    /**
     *
     * 根据ID查询详情
     * @param pk
     * @return
     */
    T findOneById(String pk);

    /**
     * 根据查询条件统计总记录数据
     *
     * @param condition
     * @return
     */
    Long count(T condition);

}