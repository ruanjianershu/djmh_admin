package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.entity.Message;
import com.jlju.djmhadmin.dao.mapper.MessageMapper;
import com.jlju.djmhadmin.dao.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 留言信息管理
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 20:25
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
public class MessageServiceImpl implements MessageService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private MessageMapper messageMapper;

    @Override
    public void add(Message var1) {
        logger.info("***messageService 层添加留言操作***");
        this.messageMapper.add(var1);
    }

    @Override
    public int updateById(Message var1) {
        logger.info("***messageService 层更新留言操作***");
        return this.messageMapper.updateById(var1);
    }

    @Override
    public void deleteById(Message var1) {
        logger.info("***messageService 层删除留言操作***");
        this.messageMapper.deleteById(var1);
    }

    @Override
    public Message findOneById(String pk) {
        logger.info("***messageService 层查看详细留言操作***");
        return this.messageMapper.findOneById(pk);
    }

    @Override
    public List<Message> findAll(Message pk) {
        logger.info("***messageService 层查询全部留言操作***");
        return this.messageMapper.findAll(pk);
    }
}