package com.jlju.djmhadmin.dao.service;


import com.jlju.djmhadmin.dao.common.GenericService;
import com.jlju.djmhadmin.dao.entity.Notice;

import java.util.List;

public interface NoticeService extends GenericService<Notice>{
    List<Notice> findAdmin(Notice pk);
}
