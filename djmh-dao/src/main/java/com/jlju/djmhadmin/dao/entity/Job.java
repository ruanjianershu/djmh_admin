package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 求职信息
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:18
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {

    public Job(String uuid) {
        this.uuid = uuid;
    }

    private String uuid;
    private String name;
    private String sex;
    private String birthday;
    //求职的职位
    private String job;
    private String profession;
    private String school;
    private String education;
    private String cardId;
    private String photo;
    //简历地址
    private String resyme;

    private String email;
    private String phone;
    private int status;
    private String memo;


}