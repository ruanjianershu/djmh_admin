package com.jlju.djmhadmin.dao.entity;

import lombok.Data;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: user
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-20 10:16
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
public class User {

    public User(String acNo, String name, String email, String phone) {
        this.acNo = acNo;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    public User(String uuid, String acNo, String password, String name, String email, String phone, String status, String identity) {
        this.uuid = uuid;
        this.acNo = acNo;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.identity = identity;
    }

    public User() {
    }

    private String uuid;

    private String acNo;

    private String password;

    private String name;

    private String email;

    private String phone;

    private String status;

    /**身份 0 ： 学生 1 ： 老师 2 ： 管理员*/
    private String identity;

}