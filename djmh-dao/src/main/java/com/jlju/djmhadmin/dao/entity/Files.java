package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By Suixingpay.
 *
 * @description: 文件
 * @author: LiTing[liting08011@163.COM]
 * @create: 2018-05-03 11:10
 * @Copyright ©2018 Suixingpay. All rights reserved.
 * 注意：本内容仅限于ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Files {

    private String uuid;
    //文件名
    private String fileName;
    //文件地址
    private String url;
    //创建日期
    private Date DT_CTE;


}