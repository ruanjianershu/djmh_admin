package com.jlju.djmhadmin.dao.service.impl;

import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Job;
import com.jlju.djmhadmin.dao.mapper.JobMapper;
import com.jlju.djmhadmin.dao.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 求职信息管理
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 20:23
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Service
@Slf4j
public class JobServiceImpl implements JobService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JobMapper jobMapper;


    @Override
    public void add(Job var1) {
        logger.info("***求职信息管理-添加service层***");
        var1.setUuid(UUIDUtils.getUuid());
        jobMapper.add(var1);
    }

    @Override
    public int updateById(Job var1) {
        logger.info("***求职信息管理-更新service层***");
        return this.jobMapper.updateById(var1);
    }

    @Override
    public void deleteById(Job var1) {
        logger.info("***求职信息管理-删除service层***");
        this.jobMapper.deleteById(var1);
    }

    @Override
    public Job findOneById(String pk) {
        logger.info("***求职信息管理-查看详情service层***");
        return this.jobMapper.findOneById(pk);
    }

    @Override
    public List<Job> findAll(Job pk) {
        logger.info("***求职信息管理-查看全部申请service层***");
        return this.jobMapper.findAll(pk);
    }

    @Override
    public Job getById(Job job) {
        logger.info("***求职信息管理-验证申请service层***");
        return this.jobMapper.getById(job);
    }
}