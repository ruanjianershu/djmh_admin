package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 学生请假相关信息
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-18 16:18
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stuents {

    public Stuents(String uuid) {
        this.uuid = uuid;
    }

    private String uuid;
    private String name;
    private String acNo;
    private String className;
    private String cause;
    private String startDate;
    private String endDate;
    private String days;
    //审批人
    private String approver;
    private String status;

}