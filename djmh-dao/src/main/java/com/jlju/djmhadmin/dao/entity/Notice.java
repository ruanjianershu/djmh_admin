package com.jlju.djmhadmin.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 新闻公告等通用实体bean
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-25 10:24
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notice {

    public Notice(String uuid) {
        this.uuid = uuid;
    }

    private String uuid;
    private String title;
    private String content;
    private String publishDate;
    private String author;
    /**类型 0： 新闻  1 ：公告 2 ： 比赛信息*/
    private String type;
    /**0 ： 删除   1 ：正常*/
    private String status;

}