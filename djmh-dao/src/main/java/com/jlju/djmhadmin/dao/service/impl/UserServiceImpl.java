/**
 * All rights Reserved, Designed By Liting.
 *
 * @author: liyang[li_yang@Liting.com]
 * @date: 2017年10月25日 上午10:09:22
 * @Copyright ©2017 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 */
package com.jlju.djmhadmin.dao.service.impl;


import com.jlju.djmhadmin.dao.common.util.DataEncryptUtil;
import com.jlju.djmhadmin.dao.common.util.EmailUtil;
import com.jlju.djmhadmin.dao.common.util.MailUtil;
import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.User;
import com.jlju.djmhadmin.dao.mapper.UserMapper;
import com.jlju.djmhadmin.dao.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

/**
 * Created by gz on 2017/11/7
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(String account, String password) {
        logger.info("用户登陆service层");
        User user = userMapper.login(account, password);
        //判断
//        try {
//            if (user == null) {
//                //抛出异常
//                throw new Exception("用户名或密码错误");
//            }
//        } catch (Exception e) {
//            logger.error("***service层抛错：用户名或密码错误***");
//        }
        return user;
    }

    @Override
    public String forget(String account) throws Exception {
        //更新数据库密码
        User user = userMapper.findByAccount(account);
        if (user != null) {
            String pass = UUIDUtils.getUuid();
            user.setPassword(pass);
            user.setPassword(DataEncryptUtil.encryptBASE64(user.getPassword().getBytes()));
            //创建邮件
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
            props.setProperty("mail.smtp.host", EmailUtil.myEmailSMTPHost);   // 发件人的邮箱的 SMTP 服务器地址
            props.setProperty("mail.smtp.auth", "false");            // 需要请求认证
            // 用于连接邮件服务器的参数配置（发送邮件时才需要用到）
            Session session = Session.getInstance(props);        // 根据参数配置，创建会话对象（为了发送邮件准备的）
            MimeMessage message = MailUtil.createMimeMessage(session, EmailUtil.myEmailAccount, user.getEmail(), pass);    // 创建邮件对象
            // 4. 根据 Session 获取邮件传输对象
            Transport transport = session.getTransport();
            transport.connect(EmailUtil.myEmailAccount, EmailUtil.myEmailPassword);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            try {
                userMapper.updateById(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "SUCCESS";
        } else {
            return "用户不存在";
        }
    }


    @Override
    public void password(User user) {
        userMapper.password(user);
    }

    @Override
    public void add(User var1) {
        logger.info("新增内部用户service层");
        //新用户注册，默认状态为可用
        var1.setStatus("1");
        var1.setUuid(UUIDUtils.getUuid());
        this.userMapper.add(var1);
        logger.info("内部用户新增成功");
    }

    @Override
    public int updateById(User var1) {
        logger.info("登陆用户信息更新");
        return this.userMapper.updateById(var1);
    }

    @Override
    public void deleteById(User var1) {
        logger.info("登陆用户状态更新");
        this.userMapper.deleteById(var1);
    }

    @Override
    public User findOneById(String pk) {
        logger.info("获取登陆用户详细信息");
        return this.userMapper.findOneById(pk);
    }

    @Override
    public List<User> findAll(User pk) {
        logger.info("***获取全部用户service层进入***");
        return this.userMapper.findAll(pk);
    }
}