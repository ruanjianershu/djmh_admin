#!/bin/sh
JAVA_HOME=/home/app/jdk1.8.0_101
APP_HOME=/home/app/sxpservice
APP_CLASSPATH=$APP_HOME/app
APP_CLASSPATH=$APP_CLASSPATH:` find $APP_HOME/lib -name *.jar|tr '\n' ':'`
export JAVA_HOME APP_HOME APP_CLASSPATH
CURRENT_DATE=`date +%Y%m%d%H%M%S`
$JAVA_HOME/bin/java -javaagent:$APP_HOME/lib/springloaded-1.2.4.RELEASE.jar -noverify -server -Xmx4024M -Xms4024M -Xmn2048M  -XX:SurvivorRatio=2 -Xss256K -XX:PermSize=512M -XX:MaxPermSize=512M  -XX:+ExplicitGCInvokesConcurrent -XX:+UseCMSInitiatingOccupancyOnly -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=0 -XX:+CMSClassUnloadingEnabled -XX:LargePageSizeInBytes=128M -XX:+UseFastAccessorMethods -XX:CMSInitiatingOccupancyFraction=70 -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+PrintClassHistogram -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintHeapAtGC -Xloggc:$APP_HOME/logs/gc_$CURRENT_DATE.log -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$APP_HOME/temp/ -XX:OnOutOfMemoryError=$APP_HOME/bin/restart.sh -Dcom.sun.management.jmxremote.port=26666 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false  -jar $APP_HOME/app/service-1.0.0.jar --profile=rc >$APP_HOME/logs/startup.log 2>&1 &