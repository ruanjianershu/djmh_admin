/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 127.0.0.1
 Source Database       : djmh

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : utf-8

 Date: 04/18/2018 21:03:05 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `DJMH_ REQUIRE_JOB`
-- ----------------------------
DROP TABLE IF EXISTS `DJMH_ REQUIRE_JOB`;
CREATE TABLE `DJMH_ REQUIRE_JOB` (
  `UUID` varchar(200) NOT NULL COMMENT 'UUID 随机生成',
  `NAME` varchar(60) NOT NULL COMMENT '姓名',
  `SEX` varchar(30) NOT NULL COMMENT '性别',
  `BIRTHDAY` varchar(300) DEFAULT NULL COMMENT '出生日期',
  `JOB` varchar(128) NOT NULL COMMENT '应聘职位',
  `PROFESSION` varchar(256) NOT NULL COMMENT '所学专业',
  `SCHOOL` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '毕业院校',
  `MEMO` varchar(512) DEFAULT NULL COMMENT '备注',
  `EDUCATION` varchar(256) NOT NULL COMMENT '最高学历',
  `CARD_ID` varchar(64) NOT NULL COMMENT '身份证号',
  `PHOTO` varchar(256) DEFAULT NULL COMMENT '照片路径',
  `RESYME` varchar(256) NOT NULL COMMENT '简历地址',
  `EMAIL` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(16) DEFAULT NULL,
  `STATUS` int(2) DEFAULT NULL COMMENT '处理状态 0 处理中 1 PASS 2 通过 3 待入职',
  KEY `JOB` (`JOB`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `DJMH_LOGIN`
-- ----------------------------
DROP TABLE IF EXISTS `DJMH_LOGIN`;
CREATE TABLE `DJMH_LOGIN` (
  `UUID` varchar(255) NOT NULL COMMENT 'uuid 系统自动生成',
  `AC_NO` varchar(255) NOT NULL COMMENT '登陆账号(学号工号)',
  `PASSWORD` varchar(255) NOT NULL COMMENT '密码',
  `EMAIL` varchar(128) NOT NULL COMMENT '邮箱',
  `PHONE` varchar(11) DEFAULT NULL COMMENT '手机号',
  `STATUS` int(2) NOT NULL DEFAULT '1' COMMENT '0 注释 1 正常',
  `IDENTITY` int(2) NOT NULL,
  PRIMARY KEY (`AC_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `DJMH_LOGIN`
-- ----------------------------
BEGIN;
INSERT INTO `DJMH_LOGIN` VALUES ('qvakildfjnvaer12', 'a', 'a', '1', '1', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `DJMH_ONLINE_MESSAGE`
-- ----------------------------
DROP TABLE IF EXISTS `DJMH_ONLINE_MESSAGE`;
CREATE TABLE `DJMH_ONLINE_MESSAGE` (
  `UUID` varchar(128) NOT NULL COMMENT 'uuid 系统随机生成',
  `NAME` varchar(64) NOT NULL COMMENT '留言人姓名',
  `PHONE` varchar(32) DEFAULT '1' COMMENT '手机号',
  `EMAIL` varchar(64) DEFAULT '1' COMMENT '邮箱',
  `IP_ADDRESS` varchar(128) NOT NULL COMMENT 'IP 地址',
  `MESSAGE` varchar(1280) NOT NULL COMMENT '留言信息'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `DJMH_STUDENTS_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `DJMH_STUDENTS_INFO`;
CREATE TABLE `DJMH_STUDENTS_INFO` (
  `UUID` varchar(256) NOT NULL COMMENT 'UUID 系统随机生成',
  `NAME` varchar(128) NOT NULL COMMENT '姓名',
  `SEX` varchar(64) NOT NULL COMMENT '性别',
  `CLASS_NAME` varchar(128) NOT NULL COMMENT '班级',
  `AC_NO` varchar(64) NOT NULL COMMENT '学号',
  `PAR_PHONE` varchar(64) NOT NULL COMMENT '家长手机号',
  `PHONE` varchar(64) NOT NULL COMMENT '个人手机号',
  `QQ` varchar(64) NOT NULL,
  `CARD_ID` varchar(64) NOT NULL COMMENT '身份证号',
  `JOB_CITY` varchar(64) DEFAULT NULL COMMENT '工作城市',
  `EMAIL` varchar(64) NOT NULL COMMENT '邮箱',
  `NATIONAL` varchar(128) NOT NULL COMMENT '籍贯',
  `STATUS` varchar(4) NOT NULL COMMENT '状态',
  `PHOTO` varchar(64) NOT NULL COMMENT '照片'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `DJMH_STUENTS_LEVEL`
-- ----------------------------
DROP TABLE IF EXISTS `DJMH_STUENTS_LEVEL`;
CREATE TABLE `DJMH_STUENTS_LEVEL` (
  `UUID` varchar(256) NOT NULL COMMENT 'UUID 系统随机生成',
  `NAME` varchar(128) DEFAULT NULL COMMENT '姓名',
  `AC_NO` varchar(128) DEFAULT NULL COMMENT '学号',
  `CLASS_NAME` varchar(128) DEFAULT NULL COMMENT '班级',
  `CAUSE` varchar(128) DEFAULT NULL COMMENT '事由',
  `START_DATE` varchar(128) DEFAULT NULL COMMENT '开始日期',
  `END_DATE` varchar(128) DEFAULT NULL COMMENT '结束日期',
  `DAYS` varchar(64) DEFAULT NULL COMMENT '天数',
  `APPROVER` varchar(128) DEFAULT NULL COMMENT '审批人'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
