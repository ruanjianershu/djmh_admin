package com.jlju.djmhadmin.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jlju.djmhadmin.common.GenericResponse;
import com.jlju.djmhadmin.common.util.JsonUtil;
import com.jlju.djmhadmin.common.util.RequiredStringValidator;
import com.jlju.djmhadmin.common.util.StringUtils;
import com.jlju.djmhadmin.common.util.Validations;
import com.jlju.djmhadmin.dao.entity.Job;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.service.JobService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 求职相关controller
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-19 17:52
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@RestController
@Slf4j
@RequestMapping("/job")
public class JobController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JobService jobService;


    @ApiOperation("新增工作申请")
    @RequestMapping(value = "/add")
    @Validations(requiredStrings={
            @RequiredStringValidator(fieldName="name",errCode="ACM0204",errMsg="请求参数不足,姓名不能为空"),
            @RequiredStringValidator(fieldName="sex",errCode="ACM0205",errMsg="请求参数不足，性别不能为空"),
            @RequiredStringValidator(fieldName="birthday",errCode="ACM0500",errMsg="请求参数不足，生日不能为空"),
            @RequiredStringValidator(fieldName="job",errCode="ACM0501",errMsg="请求参数不足，目标岗位不能为空"),
            @RequiredStringValidator(fieldName="profession",errCode="ACM0502",errMsg="请求参数不足，专业不能为空"),
            @RequiredStringValidator(fieldName="school",errCode="ACM0503",errMsg="请求参数不足，毕业院校不能为空"),
            @RequiredStringValidator(fieldName="education",errCode="ACM0504",errMsg="请求参数不足，学历不能为空"),
            @RequiredStringValidator(fieldName="cardId",errCode="ACM0505",errMsg="请求参数不足，身份证号不能为空"),
            @RequiredStringValidator(fieldName="email",errCode="ACM0507",errMsg="请求参数不足，邮箱不能为空"),
            @RequiredStringValidator(fieldName="phone",errCode="ACM0508",errMsg="请求参数不足，手机号不能为空")
    })
    public Callable<GenericResponse<String>> add(@RequestBody String json) {
        logger.info("***新增工作申请添加开始*** 入参：" + json);
        try {
            Job job = JsonUtil.jsonToObject(json,Job.class);
            //验证是否申请过且在审批中
            if(StringUtils.isEmpty(this.jobService.getById(job))){
                throw new Exception("已经申请过该职位，不允许重复申请");
            }
            this.jobService.add(job);
            logger.info("***新增工作申请添加成功***" + job);
            //向求职者发送邮件
            return () -> GenericResponse.success("ACM0008", "添加成功", job);
        } catch (Exception e) {
            logger.error("***新增工作申请添加失败***" + e);
            return () -> GenericResponse.failed("ACM0009", "添加失败");
        }
    }

    @RequiredStringValidator(fieldName="uuid",trim=true,errCode="ACM0509",errMsg="请求参数不足,申请单号不能为空")
    @ApiOperation("删除工作申请")
    @RequestMapping(value = "/delete")
    public Callable<GenericResponse<String>> delete(@RequestBody String json) {
        logger.info("***工作申请删除开始***入参：" + json);
        try {
            json = StringUtils.trimFirstAndLastChar(json,'\"');
            Job job = new Job(json);
            this.jobService.deleteById(job);
            logger.info("***工作申请删除成功***" + job);
            return () -> GenericResponse.success("ACM0300", "删除成功", job);
        } catch (Exception e) {
            logger.error("***工作申请删除失败***" + e);
            return () -> GenericResponse.failed("ACM0301", "删除失败");
        }
    }

    @RequiredStringValidator(fieldName="uuid",trim=true,errCode="ACM0509",errMsg="请求参数不足,申请单号不能为空")
    @ApiOperation("更新工作申请")
    @RequestMapping(value = "/update")
    public Callable<GenericResponse<String>> update(@RequestBody String json) {
        logger.info("***工作申请更新开始***");
        try {
            Job job = JsonUtil.jsonToObject(json,Job.class);
            this.jobService.updateById(job);
            logger.info("***工作申请更新成功***" + job);
            return () -> GenericResponse.success("ACM0014", "更新成功", job);
        } catch (Exception e) {
            logger.error("***工作申请更新失败***" + e);
            return () -> GenericResponse.failed("ACM0015", "更新成功");
        }
    }

    @ApiOperation("查看全部工作申请，带分页")
    @RequestMapping(value = "/findAll")
    public Callable<GenericResponse<List<Job>>> findAll(@RequestBody String json, int pageNum, int pageSize) {
        logger.info("***查看全部工作申请信息（分页）***");
        try {
            Job job = JsonUtil.jsonToObject(json,Job.class);
            PageHelper.startPage(pageNum, pageSize);
            List<Job> list = this.jobService.findAll(job);
            // 用PageInfo对结果进行包装
            PageInfo<Job> pageInfo = new PageInfo<Job>(list);
            logger.info("***查看全部工作申请信息（分页） 成功***");
            return () -> GenericResponse.success("ACM0010", "查询成功", pageInfo);
        } catch (Exception e) {
            logger.error("***查看全部工作申请信息（分页）失败***" + e);
            return () -> GenericResponse.failed("ACM0011", "查询失败");
        }
    }

    @ApiOperation("查看详细工作申请信息")
    @RequestMapping(value = "/findOneById")
    public Callable<GenericResponse<String>> findOneById(@RequestBody String json) {
        logger.info("***工作申请信息查询开始***");
        try {
            Job job = JsonUtil.jsonToObject(json,Job.class);
            Job finalJob = this.jobService.findOneById(job.getUuid());
            logger.info("***工作申请信息查询成功***");
            return () -> GenericResponse.success("ACM0012", "留言信息查询成功", finalJob);
        } catch (Exception e) {
            logger.error("***工作申请信息查询失败***" + e);
            return () -> GenericResponse.failed("ACM0012", "留言信息查询失败");
        }
    }

    @RequiredStringValidator(fieldName="uuid",trim=true,errCode="ACM0509",errMsg="请求参数不足,申请单号不能为空")
    @ApiOperation("更新工作申请")
    @RequestMapping(value = "/next")
    public Callable<GenericResponse<String>> next(@RequestBody String json) {
        logger.info("***工作申请更新开始***入参：" + json);
        try {
            json = StringUtils.trimFirstAndLastChar(json,'\"');
            Job job = new Job(json);
            int status = this.jobService.findOneById(json).getStatus();
            if(5!=status){
                status++;
            }
            job.setStatus(status);
            this.jobService.updateById(job);
            logger.info("***工作申请更新成功***" + job);
            return () -> GenericResponse.success("ACM0014", "更新成功", job);
        } catch (Exception e) {
            logger.error("***工作申请更新失败***" + e);
            return () -> GenericResponse.failed("ACM0015", "更新失败");
        }
    }
}