//package com.jlju.djmhadmin.controller;
//
//import com.jlju.djmhadmin.dao.service.TestService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
///**
// * All rights Reserved, Designed By Liting.
// *
// * @description: controller
// * @author: LiTing[liting08011@163.com]
// * @create: 2018-04-20 10:02
// * @Copyright ©2018 Liting. All rights reserved.
// * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
// **/
//@RestController
//@RequestMapping("/")
//@Api("userController相关api")
//public class TestController {
//
//    @Autowired
//    private TestService testService;
//
//    @ApiOperation("获取用户信息")
//    @GetMapping()
//    public String test(){
////        String a = "";
////        return "\"aaaa\"";
//        return testService.find().toString();
//    }
//}