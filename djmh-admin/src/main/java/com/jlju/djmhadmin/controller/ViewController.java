package com.jlju.djmhadmin.controller;

import com.jlju.djmhadmin.dao.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * 〈界面控制〉
 * 〈功能详细描述〉
 *
 * @author lit
 * @version [版本号, YYYY-MM-DD]
 * @date 2017/11/8
 * @see [相关类/方法]
 */
@Slf4j
@Controller
public class ViewController {

    @Autowired
    UserService userService;

    /**
     * 欢迎界面
     *
     * @return
     */
    @GetMapping("/wel")
    public String welcome() {
        return "djmh/index";
    }


    /** 
    * @Description:
    * @Param:  
    * @return:  
    * @Author: LiTing [liting08011@163.com]
    * @Date: 2018/4/18 
    * @Time: 下午4:18
    */ 
    @GetMapping("/userList")
    public String goUserList() {
        return "djmh/userList";
    }
    
    /**
     * 跳转到用户新增页面
     *
     * @return
     */
    @RequestMapping(value = "/user/goPassword")
    public ModelAndView goPassword(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("password");
        return mv;
    }

    /**
     * 跳转到用户新增页面
     *
     * @return
     */
    @RequestMapping(value = "/user/goAdd")
    public ModelAndView goAdd(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("update");
        return mv;
    }


    /**
     * 跳转到查看页面
     *
     * @return
     */
    @RequestMapping(value = "/goSys")
    public ModelAndView goSys() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("sys");
        return mv;
    }

    /**
     * 生成验证码
     *
     * @throws IOException
     */
    @RequestMapping("/createImage")
    public void createImage(HttpServletResponse response, HttpSession session) throws IOException {
        BufferedImage image = new BufferedImage(80, 30, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        Random r = new Random();
        g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        g.fillRect(0, 0, 80, 30);
        //获取生成的验证码
        String code = getNumber();
        //绑定验证码
        session.setAttribute("number", code);
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
        g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        g.drawString(code, 5, 25);
        //设置消息头
        response.setContentType("image/jpeg");
        OutputStream os = response.getOutputStream();
        ImageIO.write(image, "jpeg", os);
    }

    public String getNumber() {
        String str = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String code = "";
        for (int i = 0; i < 4; i++) {
            int index = (int) (Math.random() * str.length());
            code += str.charAt(index);
        }
        return code;
    }
}