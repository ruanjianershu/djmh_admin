package com.jlju.djmhadmin.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jlju.djmhadmin.common.GenericResponse;
import com.jlju.djmhadmin.common.util.JsonUtil;
import com.jlju.djmhadmin.common.util.RequiredStringValidator;
import com.jlju.djmhadmin.common.util.Validations;
import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.entity.Message;
import com.jlju.djmhadmin.dao.service.MessageService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: 在线留言controller类
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-19 17:50
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@RestController
@Slf4j
@RequestMapping("/message")
public class MessageController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageService messageService;

    @ApiOperation("新增留言")
    @RequestMapping(value = "/add")
    @Validations(requiredStrings={
            @RequiredStringValidator(fieldName="name",errCode="ACM0204",errMsg="请求参数不足,姓名不能为空"),
            @RequiredStringValidator(fieldName="message",errCode="ACM0400",errMsg="请求参数不足，留言信息不能为空")
    })
    public Callable<GenericResponse<String>> add(@RequestBody String json) {
        logger.info("***新闻留言添加开始***");
        try {
            Message message = JsonUtil.jsonToObject(json,Message.class);
            message.setUuid(UUIDUtils.getUuid());
            message.setIp("128.0.0.1");
            this.messageService.add(message);
            logger.info("***留言添加成功***" + message);
            return () -> GenericResponse.success("ACM0008", "添加成功", message);
        } catch (Exception e) {
            logger.error("***留言添加失败***" + e);
            return () -> GenericResponse.failed("ACM0009", "添加失败");
        }
    }

    @RequiredStringValidator(fieldName="uuid",trim=true,errCode="ACM0401",errMsg="请求参数不足,留言单号不能为空")
    @ApiOperation("删除留言")
    @RequestMapping(value = "/delete")
    public Callable<GenericResponse<String>> delete(@RequestBody String json) {
        logger.info("***留言删除开始***");
        try {
            Message message = JsonUtil.jsonToObject(json,Message.class);
            this.messageService.deleteById(message);
            logger.info("***留言删除成功***" + message);
            return () -> GenericResponse.success("ACM0300", "删除成功", message);
        } catch (Exception e) {
            logger.error("***留言删除失败***" + e);
            return () -> GenericResponse.failed("ACM0301", "删除失败");
        }
    }

    @RequiredStringValidator(fieldName="uuid",trim=true,errCode="ACM0401",errMsg="请求参数不足,留言单号不能为空")
    @ApiOperation("更新留言")
    @RequestMapping(value = "/update")
    public Callable<GenericResponse<String>> update(@RequestBody String json) {
        logger.info("***留言更新开始***");
        try {
            Message message = JsonUtil.jsonToObject(json,Message.class);
            this.messageService.updateById(message);
            logger.info("***留言更新成功***" + message);
            return () -> GenericResponse.success("ACM0014", "更新成功", message);
        } catch (Exception e) {
            logger.error("***留言更新失败***" + e);
            return () -> GenericResponse.failed("ACM0015", "更新成功");
        }
    }

    @ApiOperation("查看全部留言，带分页")
    @RequestMapping(value = "/findAll")
    public Callable<GenericResponse<List<Message>>> findAll(@RequestBody String json, int pageNum, int pageSize) {
        logger.info("***查看全部留言信息（分页）***");
        try {
            Message message = JsonUtil.jsonToObject(json,Message.class);
            PageHelper.startPage(pageNum, pageSize);
            List<Message> list = this.messageService.findAll(message);
            // 用PageInfo对结果进行包装
            PageInfo<Message> pageInfo = new PageInfo<Message>(list);
            logger.info("***查看全部留言信息（分页） 成功***");
            return () -> GenericResponse.success("ACM0010", "查询成功", pageInfo);
        } catch (Exception e) {
            logger.error("***查看全部留言信息（分页）失败***" + e);
            return () -> GenericResponse.failed("ACM0011", "查询失败");
        }
    }

    @ApiOperation("查看详细留言信息")
    @RequestMapping(value = "/findOneById")
    public Callable<GenericResponse<String>> findOneById(@RequestBody String json) {
        logger.info("***留言信息查询开始***");
        try {
            Message message = JsonUtil.jsonToObject(json,Message.class);
            Message finalMessage = this.messageService.findOneById(message.getUuid());
            logger.info("***留言信息查询成功***");
            return () -> GenericResponse.success("ACM0012", "留言信息查询成功", finalMessage);
        } catch (Exception e) {
            logger.error("***留言信息查询失败***" + e);
            return () -> GenericResponse.failed("ACM0012", "留言信息查询失败");
        }
    }
}