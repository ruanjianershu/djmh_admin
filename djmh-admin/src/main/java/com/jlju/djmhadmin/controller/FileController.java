package com.jlju.djmhadmin.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jlju.djmhadmin.common.GenericResponse;
import com.jlju.djmhadmin.common.util.JsonUtil;
import com.jlju.djmhadmin.common.util.StringUtils;
import com.jlju.djmhadmin.dao.common.util.UUIDUtils;
import com.jlju.djmhadmin.dao.entity.Files;
import com.jlju.djmhadmin.dao.entity.Job;
import com.jlju.djmhadmin.dao.entity.Student;
import com.jlju.djmhadmin.dao.service.FilesService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * All rights Reserved, Designed By Suixingpay.
 *
 * @description: 对上传的文件操作
 * @author: LiTing[li_ting1@suixingpay.com]
 * @create: 2018-05-03 11:13
 * @Copyright ©2018 Suixingpay. All rights reserved.
 * 注意：本内容仅限于随行付支付有限公司内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@RestController
@Slf4j
@RequestMapping("/file")
public class FileController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private FilesService filesService;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Callable<GenericResponse<String>> upload(@RequestBody String json) {
        logger.info("***新增文件申请添加开始*** 入参：" + json);
        try {
            Files files = JsonUtil.jsonToObject(json,Files.class);
            files.setUuid(UUIDUtils.getUuid());
            files.setDT_CTE(new Date());
            this.filesService.add(files);
            logger.info("***新增文件添加成功***" + files);
            //向求职者发送邮件
            return () -> GenericResponse.success("ACM0008", "添加成功", files);
        } catch (Exception e) {
            logger.error("***新增文件添加失败***" + e);
            return () -> GenericResponse.failed("ACM0009", "添加失败");
        }
    }


    @ApiOperation("查看全部文件，带分页")
    @RequestMapping(value = "/findAll")
    public Callable<GenericResponse<List<Files>>> findAll(@RequestBody String json, int pageNum, int pageSize) {
        logger.info("***查看全部文件信息（分页）***");
        try {
            Files files = JsonUtil.jsonToObject(json,Files.class);
            PageHelper.startPage(pageNum, pageSize);
            List<Files> list = this.filesService.findAll(files);
            // 用PageInfo对结果进行包装
            PageInfo<Files> pageInfo = new PageInfo<Files>(list);
            logger.info("***查看全部文件信息（分页） 成功***");
            return () -> GenericResponse.success("ACM0010", "查询成功", pageInfo);
        } catch (Exception e) {
            logger.error("***查看全部文件信息（分页）失败***" + e);
            return () -> GenericResponse.failed("ACM0011", "查询失败");
        }
    }

}