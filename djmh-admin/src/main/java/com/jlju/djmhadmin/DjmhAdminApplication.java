package com.jlju.djmhadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.concurrent.LinkedBlockingDeque;

@SpringBootApplication()
//@EnableDiscoveryClient
@MapperScan({"com.jlju.djmhadmin.dao.mapper"})
public class DjmhAdminApplication {
	private static final Logger logger = LoggerFactory.getLogger(DjmhAdminApplication.class);

	private static LinkedBlockingDeque<Boolean> RUN = new LinkedBlockingDeque<Boolean>();

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DjmhAdminApplication.class);
	}

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		try {
			SpringApplication springApplication = new SpringApplication(DjmhAdminApplication.class);
			springApplication.run(args);
			// logger.info(print());
			System.out.println(print());
			logger.info("扫描完成");
			System.out.println("电计门户启动成功");
			logger.info("电计门户启动成功");
			while (RUN.take()) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Stop.
	 */
	static void stop() {
		RUN.add(false);
	}

	private static String print() {
		StringBuffer sb = new StringBuffer();
		sb.append("                   _ooOoo_\n");
		sb.append("                  o8888888o\n");
		sb.append("                  88\" . \"88\n");
		sb.append("                  (| ^_^ |)\n");
		sb.append("                  O\\  =  /O\n");
		sb.append("               ____/`---'\\____\n");
		sb.append("             .'  \\\\|     |//  `.\n");
		sb.append("            /  \\\\|||  :  |||//  \\ \n");
		sb.append("           /  _||||| -:- |||||-  \\ \n");
		sb.append("           |   | \\\\\\  -  /// |   |\n");
		sb.append("           | \\_|  ''\\---/''  |   |\n");
		sb.append("           \\  .-\\__  `-`  ___/-. /\n");
		sb.append("         ___`. .'  /--.--\\  `. . __\n");
		sb.append("      .\"\" '<  `.___\\_<|>_/___.'  >'\"\".\n");
		sb.append("     | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |\n");
		sb.append("     \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /\n");
		sb.append("======`-.____`-.___\\_____/___.-`____.-'======\n");
		sb.append("                   `=---='\n");
		sb.append("...................................................\n");
		return sb.toString();
	}
}
