package com.jlju.djmhadmin.common.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: String util
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-23 下午2:00:45
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
public class StringUtils {
    /**
     * String对象是否为空
     * 创 建 人:  liting
     * 创建时间:  2018年4月23日 下午2:13:45  
     * @param value
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmpty(String value) {
        return (value == null || value.trim().equals(""));
    }


    public static boolean isNullOrEmpty(String value){
        return isEmpty(value);
    }
    /**
     * Object对象是否为空
     * 创 建 人:  liting
     * 创建时间:  2018年4月23日 上午11:43:52  
     * @param value
     * @return true：为空，false：不为空
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmpty(Object value) {
        return value==null?true:false;
    }

    /**
     * BigDecimal类型值是否为空
     *    若果value值小于或者等0时返回true，认为为空
     * 创 建 人:  liting
     * 创建时间:  2018年4月23日 下午12:53:47  
     * @param value
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmpty(BigDecimal value){
        if(value==null) return true;
        BigDecimal initBig=new BigDecimal("0");
        if(initBig.compareTo(value)==0||value.compareTo(initBig)<=0){
            return true;
        }
        return false;
    }

    /**
     * List元素是否为空
     * 创 建 人:  liting
     * 创建时间:  2018年4月24日 下午5:29:16
     * @param list
     * @return true：为空，false：不为空
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(List list) {
        if (list == null || list.size() == 0)
            return true;
        else
            return false;
    }

    /**
     * <功能详细描述>
     * <p>创 建 人：  liting<br>
     * 创建时间：  2018年4月29日 下午2:00:07
     * @param obj
     * @param fieldName
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static Object getFieldValue(final Object obj, String fieldName) {
        Object val = null;
        try {
            Method[] method = obj.getClass().getDeclaredMethods();
            for (int index = 0; index < method.length; index++) {
                if (method[index].getName().equalsIgnoreCase("get" + fieldName)) {
                    val = method[index].invoke(obj, null);
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return val;
    }

    /**
     * 去除字符串首尾出现的某个字符.
     * @param source 源字符串.
     * @param element 需要去除的字符.
     * @return String.
     */
    public static String trimFirstAndLastChar(String source,char element){
        boolean beginIndexFlag = true;
        boolean endIndexFlag = true;
        do{
            int beginIndex = source.indexOf(element) == 0 ? 1 : 0;
            int endIndex = source.lastIndexOf(element) + 1 == source.length() ? source.lastIndexOf(element) : source.length();
            source = source.substring(beginIndex, endIndex);
            beginIndexFlag = (source.indexOf(element) == 0);
            endIndexFlag = (source.lastIndexOf(element) + 1 == source.length());
        } while (beginIndexFlag || endIndexFlag);
        return source;
    }
}