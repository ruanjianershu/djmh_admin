package com.jlju.djmhadmin.common.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredStringValidator {
    String fieldName() default "";

    boolean trim() default true;

    String errMsg() default "";

    String errCode() default "";
}