package com.jlju.djmhadmin.common.Enums;

/** 
* @Description:  
* @Param:  
* @return:  
* @Author: LiTing [li_ting1@suixingpay.com] 
* @Date: 2018/4/25 
* @Time: 下午2:31
*/ 
public enum UserStatusEnums implements PersistentEnum<String> {
    /**
     * N=正常
     */

    normal("1", "正常"),
    /**
     * U=已注销
     */
    unavailable("0", "已注销");

    private String value;
    private String displayName;

    UserStatusEnums(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    @Override
    public String getDisplayName() {
        // TODO Auto-generated method stub
        return displayName;
    }
}