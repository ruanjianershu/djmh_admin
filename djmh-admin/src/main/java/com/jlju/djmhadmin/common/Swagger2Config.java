package com.jlju.djmhadmin.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * All rights Reserved, Designed By Liting.
 *
 * @description: Swagger2配置类
 * @author: LiTing[liting08011@163.com]
 * @create: 2018-04-23 10:41
 * @Copyright ©2018 Liting. All rights reserved.
 * 注意：本内容仅限于吉林建筑大学ACM协会内部传阅，禁止外泄以及用于其他的商业用途。
 **/
@Configuration
@EnableSwagger2
public class Swagger2Config {


    public static final String GROUP_NAME = "xxx接口";

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)//
                .groupName(GROUP_NAME)//
                .genericModelSubstitutes(DeferredResult.class)//
                .useDefaultResponseMessages(false)//
                .forCodeGeneration(true)//
                .pathMapping("/")//
                .select()//
                .apis(RequestHandlerSelectors.basePackage("com.jlju"))//
                .build()//
                // .securitySchemes(newArrayList(oauth()))
                // .securityContexts(newArrayList(securityContext()))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("李廷", "ruanjianershu.github.io", "liting08011@163.com");

        ApiInfo apiInfo = new ApiInfoBuilder().title("吉林建筑大学").description("电气与计算机学院门户网站").version("0.1").contact(contact)
                .termsOfServiceUrl("ruanjianershu.github.io").build();

        return apiInfo;
    }
}