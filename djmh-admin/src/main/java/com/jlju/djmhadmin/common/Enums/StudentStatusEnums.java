package com.jlju.djmhadmin.common.Enums;

/** 
* @Description:  
* @Param:  
* @return:  
* @Author: LiTing [li_ting1@suixingpay.com] 
* @Date: 2018/4/25 
* @Time: 下午2:31
*/ 
public enum StudentStatusEnums implements PersistentEnum<String> {
    /**
     * N=正常
     */

    beReported("0", "待报道"),
    /**
     * N=正常
     */

    reading("1", "在读"),
    /**
     * N=正常
     */

    leaveSchool("2", "休学"),
    /**
     * N=正常
     */

    dropSchool("3", "退学"),
    /**
     * U=已注销
     */
    graduation("4", "毕业");

    private String value;
    private String displayName;

    StudentStatusEnums(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    @Override
    public String getDisplayName() {
        // TODO Auto-generated method stub
        return displayName;
    }
}