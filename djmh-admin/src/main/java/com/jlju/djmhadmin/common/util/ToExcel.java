package com.jlju.djmhadmin.common.util;

import com.jlju.djmhadmin.dao.entity.Student;
import org.apache.poi.hssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by LiTing on 2017/8/9.
 */
public class ToExcel {

    public static void createExcel(List<Student> list, OutputStream out, HttpServletResponse response) throws FileNotFoundException {

        // 创建一个Excel文件
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 创建一个工作表
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "sheet0");//设置工作表名
        // 添加表头行
        HSSFRow hssfRow = sheet.createRow(0);
        // 设置单元格格式居中
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 声明一个画图的顶级管理器
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        // 定义注释的大小和位置,详见文档
        HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
                0, 0, 0, (short) 4, 2, (short) 6, 5));
        // 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.
        comment.setAuthor("liTing");

        // 添加表头内容
        /** 姓名*/
        HSSFCell headCell = hssfRow.createCell(0);
        headCell.setCellValue("姓名");
        headCell.setCellStyle(cellStyle);
        /** 性别*/
        headCell = hssfRow.createCell(1);
        headCell.setCellValue("性别");
        headCell.setCellStyle(cellStyle);
        /** 班级*/
        headCell = hssfRow.createCell(2);
        headCell.setCellValue("班级");
        headCell.setCellStyle(cellStyle);
        /** 学号*/
        headCell = hssfRow.createCell(3);
        headCell.setCellValue("学号");
        headCell.setCellStyle(cellStyle);
        /** 联系方式*/
        headCell = hssfRow.createCell(4);
        headCell.setCellValue("联系方式");
        headCell.setCellStyle(cellStyle);
        /** 家长电话*/
        headCell = hssfRow.createCell(5);
        headCell.setCellValue("家长电话");
        headCell.setCellStyle(cellStyle);
        /** 身份证号*/
        headCell = hssfRow.createCell(6);
        headCell.setCellValue("身份证号");
        headCell.setCellStyle(cellStyle);
        /** 邮箱*/
        headCell = hssfRow.createCell(7);
        headCell.setCellValue("邮箱");
        headCell.setCellStyle(cellStyle);
        /** 状态*/
        headCell = hssfRow.createCell(8);
        headCell.setCellValue("状态");
        headCell.setCellStyle(cellStyle);
        /** 报道日期*/
        headCell = hssfRow.createCell(9);
        headCell.setCellValue("报道日期");
        headCell.setCellStyle(cellStyle);
        /** 级别*/
        headCell = hssfRow.createCell(10);
        headCell.setCellValue("级别");
        headCell.setCellStyle(cellStyle);


        // 添加数据内容
        for (int i = 0; i < list.size(); i++) {
            hssfRow = sheet.createRow((int) i + 1);
            Student committeeMember = list.get(i);
            /***********************************************************/
            /****信息处理 Start**/
            /***********************************************************/

            /*** 创建单元格，并设置值*/
            HSSFCell cell = hssfRow.createCell(0);
            cell.setCellValue(committeeMember.getName());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(1);

            String sex = committeeMember.getSex();
            if (sex.equals("0")) {
                cell.setCellValue("女");
            } else if (sex.equals("1")) {
                cell.setCellValue("男");
            } else {
                cell.setCellValue("异常信息");
            }
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(2);
            cell.setCellValue(committeeMember.getClassName());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(3);
            cell.setCellValue(committeeMember.getAcNo());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(4);
            cell.setCellValue(committeeMember.getPhone());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(5);
            cell.setCellValue(committeeMember.getParPhone());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(6);
            cell.setCellValue(committeeMember.getCardId());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(7);
            cell.setCellValue(committeeMember.getEmail());
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(8);
            String status = committeeMember.getStatus();
            if (status.equals("0")) {
                cell.setCellValue("待报道");
            } else if (status.equals("1")) {
                cell.setCellValue("在读");
            } else if (status.equals("2")) {
                cell.setCellValue("休学");
            } else if (status.equals("3")) {
                cell.setCellValue("退学");
            } else if (status.equals("4")) {
                cell.setCellValue("毕业");
            } else {
                cell.setCellValue("异常");
            }
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(9);
            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String date = format1.format(committeeMember.getCreateDate());
            cell.setCellValue(date);
            cell.setCellStyle(cellStyle);

            cell = hssfRow.createCell(10);
            cell.setCellValue(committeeMember.getClassLevel());
            cell.setCellStyle(cellStyle);

            /***********************************************************/
            /****信息处理end**/
            /***********************************************************/

            /** 自动调整列宽*/
            sheet.autoSizeColumn((short)0); //调整第一列宽度
            sheet.autoSizeColumn((short)1); //调整第一列宽度
            sheet.autoSizeColumn((short)2); //调整第一列宽度
            sheet.autoSizeColumn((short)3); //调整第一列宽度
            sheet.autoSizeColumn((short)4); //调整第一列宽度
            sheet.autoSizeColumn((short)5); //调整第一列宽度
            sheet.autoSizeColumn((short)6); //调整第一列宽度
            sheet.autoSizeColumn((short)7); //调整第一列宽度
            sheet.autoSizeColumn((short)8); //调整第一列宽度
            sheet.autoSizeColumn((short)9); //调整第一列宽度
            sheet.autoSizeColumn((short)10); //调整第一列宽度
        }

        // 保存Excel文件
        try {
            /*
            File file =new File("D:\\person.xls");
            FileOutputStream fos = new FileOutputStream(file);
            //写入数据，并关闭文件
            workbook.write(fos);
            fos.close();
            */

         /*
            //将工作薄输出到输出流
             ServletOutputStream sos =response.getOutputStream();
             workBook.write(sos);
             fos.close();
        */
            /** 将Excel 文件down到本地*/
            OutputStream output=response.getOutputStream();
            response.reset();
            response.setHeader("Content-disposition", "attachment; filename=details.xls");
            response.setContentType("application/msexcel");
            workbook.write(output);
            output.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
