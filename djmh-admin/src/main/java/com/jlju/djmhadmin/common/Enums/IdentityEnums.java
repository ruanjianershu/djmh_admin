package com.jlju.djmhadmin.common.Enums;

/**
* @Description:
* @Param:
* @return:
* @Author: LiTing [li_ting1@suixingpay.com]
* @Date: 2018/4/25
* @Time: 下午2:28
*/
public enum IdentityEnums implements PersistentEnum<String> {
    /**
     * 0=正常
     */

    students("0", "学生"),
    /**
     * 1=已注销
     */
    teacher("1", "老师"),

    /**
     * 2=已注销
     */
    manager("2", "管理员"),
    /**
     * 3=已注销
     */
    superManager("3", "超级管理员");

    private String value;
    private String displayName;

    IdentityEnums(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    @Override
    public String getDisplayName() {
        // TODO Auto-generated method stub
        return displayName;
    }
}
