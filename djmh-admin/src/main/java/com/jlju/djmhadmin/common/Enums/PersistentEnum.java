package com.jlju.djmhadmin.common.Enums;

/**
 *
 * @author Qilun Jiang
 *
 * @param <T>
 */
public interface PersistentEnum<T> {

	T getValue();

	String getDisplayName();
}
