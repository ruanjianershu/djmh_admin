
package com.jlju.djmhadmin.common.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JsonUtil {
    private static Gson gson = null;

    private JsonUtil() {
    }

    public static String objectToJson(Object object) {
        String gsonString = null;
        if (gson != null) {
            gsonString = gson.toJson(object);
        }

        return gsonString;
    }

    public static <T> T jsonToObject(String gsonString, Class<T> cls) {
        T t = null;
        if (gson != null) {
            t = gson.fromJson(gsonString, cls);
        }

        return t;
    }

    public static <T> List<T> jsonToList(String json, Class<T> cls) {
        Gson gson = new Gson();
        List<T> list = new ArrayList();
        JsonArray array = (new JsonParser()).parse(json).getAsJsonArray();
        Iterator var5 = array.iterator();

        while(var5.hasNext()) {
            JsonElement elem = (JsonElement)var5.next();
            list.add(gson.fromJson(elem, cls));
        }

        return list;
    }

    public static <T> Set<T> jsonToSet(String json, Class<T> cls) {
        Gson gson = new Gson();
        Set<T> set = new HashSet();
        JsonArray array = (new JsonParser()).parse(json).getAsJsonArray();
        Iterator var5 = array.iterator();

        while(var5.hasNext()) {
            JsonElement elem = (JsonElement)var5.next();
            set.add(gson.fromJson(elem, cls));
        }

        return set;
    }

    public static <T> List<Map<String, T>> jsonToListMap(String gsonString) {
        List<Map<String, T>> list = null;
        if (gson != null) {
            list = (List)gson.fromJson(gsonString, (new TypeToken<List<Map<String, T>>>() {
            }).getType());
        }

        return list;
    }

    public static <T> Map<String, T> jsonToMap(String gsonString) {
        Map<String, T> map = null;
        if (gson != null) {
            map = (Map)gson.fromJson(gsonString, (new TypeToken<Map<String, T>>() {
            }).getType());
        }

        return map;
    }

    static {
        if (gson == null) {
            gson = new Gson();
        }

    }
}
