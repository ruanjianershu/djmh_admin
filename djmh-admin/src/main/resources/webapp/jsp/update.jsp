<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/8
  Time: 11:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form id="userFrom">
    <table border="1">
        <tr>
            <input type="hidden" id="user_id" name="user_id" value="${user.id}">
            <td align="center">员工编号：</td>
            <td>
                <input input type="text" name="account" id="account" value="${user.account}" maxlength="16"
                       placeholder="请输入员工编号" title="员工编号">
            </td>
            <td align="center">姓名：</td>
            <td>
                <input input type="text" name="name" id="name" value="${user.name}" maxlength="16" placeholder="请输入姓名"
                       title="姓名">
            </td>
        </tr>
        <tr>
            <td align="center">性别：</td>
            <td>
                <select id="sex" name="sex">
                    <option value="">请选择</option>
                    <option value="1"
                            <c:if test="${ 1 eq user.sex}">selected</c:if> >男
                    </option>
                    <option value="2"
                            <c:if test="${ 2 eq user.sex}">selected</c:if> >女
                    </option>
                </select>
            </td>
            <td align="center">中文全拼：</td>
            <td>
                <input input type="text" name="pinyin" id="pinyin" value="${user.pinyin}" maxlength="16"
                       placeholder="这里输入中文全拼" title="中文全拼">
            </td>
        </tr>
        <tr>
            <td align="center">基础角色：</td>
            <td>
                <select id="role" name="role">
                    <option value="请选择">请选择</option>
                    <option value="1" <c:if test="${ 1 eq user.role}">selected</c:if>>产品需求</option>
                    <option value="2" <c:if test="${ 2 eq user.role}">selected</c:if>>开发测试</option>
                </select>
            </td>
            <td align="center">出生日期：</td>
            <td>
                <input input type="text" name="birthday" id="birthday" value="${user.birthday}" onClick="WdatePicker()"
                       maxlength="16" placeholder="如:1900-01-01" title="出生日期">
            </td>
        </tr>
        <tr>
            <td align="center">邮箱：</td>
            <td>
                <input input type="text" name="email" id="email" value="${user.email}" maxlength="30"
                       placeholder="请输入邮箱" title="邮箱">
            </td>
            <td align="center">手机：</td>
            <td>
                <input input type="text" name="phone" id="phone" value="${user.phone}" maxlength="16"
                       placeholder="请输入手机号码" title="手机">
            </td>
        </tr>
        <tr>
            <td align="center">状态：</td>
            <td>
                <select id="state" name="state">
                    <option value="">请选择</option>
                    <option value="1" <c:if test="${ 1 eq user.state}">selected</c:if>>入职</option>
                    <option value="2" <c:if test="${ 1 eq user.state}">selected</c:if>>离职</option>
                </select>
            </td>
            <td align="center">入职时间：</td>
            <td>
                <input input type="text" name="hiredate" id="hiredate" value="${user.hiredate}" maxlength="16"
                       onClick="WdatePicker()" title="入职时间">
            </td>
        </tr>
    </table>
</form>
<input type="button" id="save" name="save" onclick="add();" value="保存"/>
<input type="button" id="close" name="close" onclick="close();" value="关闭"/>
</body>
<script language="javascript" type="text/javascript" src="/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/common.js"></script>
<script type="text/javascript">
    /* 保存 */
    function add() {
        console.log('??', ($("#userFrom").serializeArray()));
        var user = JSON.stringify($("#userFrom").serializeJson());
        var id = $("#user_id").val();
        /////////////////////////////////
        // /验证
        //////////////////////////////
        if ($("#account").val() == "") {
            alert("员工编号不能为空，请重新输入！");
            return false;
        }

        if ($("#name").val() == "") {
            alert("姓名不能为空，请重新输入！");
            return false;
        }
        var sex = $("#sex").val();  //获取Select选择的Value
        if (sex == "请选择") {
            alert("性别不能为空，请重新选择！");
            return false;
        }
        if ($("#pinyin").val() == "") {
            alert("中文全拼不能为空，请重新输入！");
            return false;
        }
        if ($("#role").val() == "") {
            alert("性别不能为空，请重新选择！");
            return false;
        }
        if ($("#birthday").val() == "") {
            alert("出生日期不能为空，请重新输入！");
            return false;
        }
        if ($("#email").val() == "") {
            alert("邮箱不能为空，请重新输入！");
            return false;
        }
        if ($("#email").val() != "") {
            var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
            var str = $("#email").val();
            if (reg.test(str) == false) {
                alert("邮箱格式错误，请检查后重新输入！");
                return false;
            }
        }
        if ($("#phone").val() == "") {
            alert("手机号不能为空，请重新输入！");
            return false;
        }
        if ($("#state").val() == "") {
            alert("状态不能为空，请重新选择！");
            return false;
        }
        if ($("#hiredate").val() == "") {
            alert("入职时间不能为空，请重新输入！");
            return false;
        }

        /////////////////////////////////
        // /验证结束
        //////////////////////////////
        if (id != "") {
            $.ajax({
                type: "post",
                url: "<%=basePath%>user/update/" + id,
                data: user,
                contentType: "application/json",
                success: function (data) {
                    if (data == "success") {
                        alert("天假成功,初始密码为123456，请及时修改");
                        window.location.href = "<%=basePath%>user/list/1";
                    } else {
                        alert(data);
                    }
                }
            })
        } else {
            $.ajax({
                type: "post",
                url: "<%=basePath%>user/add",
                data: user,
                contentType: "application/json",
                success: function (data) {
                    if (data == "success") {
                        alert("保存成功");
                        window.location.href = "<%=basePath%>user/list/1";
                    } else {
                        alert(data);
                    }
                }
            })
        }
    }

    /* 关闭 */
    function close() {
        window.location.href = "<%=basePath%>user/list";
    }
</script>
</html>