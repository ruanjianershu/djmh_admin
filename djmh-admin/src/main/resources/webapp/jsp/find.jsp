<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/8
  Time: 11:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <tr>
        <td align="center">员工编号：</td>
        <td>
            <input input type="text" name="userNumber" id="userNumber" disabled="true" value="${u.account}"
                   maxlength="16" placeholder="这里输入员工编号" title="员工编号">
        </td>
        <td align="center">姓名：</td>
        <td>
            <input input type="text" name="userName" id="userName" disabled="true" value="${u.name}" maxlength="16"
                   placeholder="请输入姓名" title="姓名">
        </td>
    </tr>
    <tr>
        <td align="center">性别：</td>
        <td>
            <select id="sex" disabled="true">
                <option value="">请选择</option>
                <option value="1" <c:if test="${'1' eq u.sex}">selected</c:if>>男</option>
                <option value="2" <c:if test="${'2' eq u.sex}">selected</c:if>>女</option>
            </select>
        </td>
        <td align="center">中文全拼：</td>
        <td>
            <input input type="text" name="pinyin" disabled="true" id="pinyin" value="${u.pinyin}" maxlength="16"
                   placeholder="请输入中文全拼" title="中文全拼">
        </td>
    </tr>
    <tr>
        <td align="center">基础角色：</td>
        <td>
            <select id="role" disabled="true">
                <option value="">请选择</option>
                <option value="1" <c:if test="${'1' eq u.role}">selected</c:if>>产品需求</option>
                <option value="2" <c:if test="${'2' eq u.role}">selected</c:if>>开发测试</option>
            </select>
        </td>
        <td align="center">出生日期：</td>
        <td>
            <input input type="text" name="birthday" id="birthday" disabled="true" value="${u.birthday}" maxlength="16"
                   placeholder="如:1900-01-01" title="出生日期">
        </td>
    </tr>
    <tr>
        <td align="center">邮箱：</td>
        <td>
            <input input type="text" name="email" id="email" disabled="true" value="${u.email}" maxlength="16"
                   placeholder="请输入邮箱" title="邮箱">
        </td>
        <td align="center">手机：</td>
        <td>
            <input input type="text" name="phone" id="phone" disabled="true" value="${u.phone}" maxlength="16"
                   placeholder="请输入手机" title="手机">
        </td>
    </tr>
    <tr>
        <td align="center">状态：</td>
        <td>
            <select id="state" disabled="true">
                <option value="">请选择</option>
                <option value="1" <c:if test="${'1' eq u.state}">selected</c:if>>入职</option>
                <option value="2" <c:if test="${'2' eq u.state}">selected</c:if>>离职</option>
            </select>
        </td>
        <td align="center">入职时间：</td>
        <td>
            <input input type="text" name="hiredate" disabled="true" id="hiredate" value="${u.hiredate}" maxlength="16"
                   placeholder="如:1900-01-01" title="入职时间">
        </td>
    </tr>
</table>
</body>
<script type="text/javascript" src="/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    /* 关闭 */
    function close() {
        window.location.href = "<%=basePath%>user/list";
    }
</script>
</html>
