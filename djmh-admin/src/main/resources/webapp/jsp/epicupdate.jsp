<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/8
  Time: 21:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form id="epicForm">
    <table align="center">
        <tr>
            <td>
                <input type="hidden" value="${epic.id}" id="epic_id" name="epic_id">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">项目:</td>
            <td>
                <input type="text" name="project" id="project" value="需求受理"/>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">问题类型</td>
            <td>
                <input type="text" name="type" id="type" value="业务问题（史诗）"/>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">Epic Name</td>
            <td>
                <input type="text" name="name" id="name" value="${epic.name}" maxlength="32" placeholder="请输入Epic Name"
                       title="Epic Name">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">主题</td>
            <td>
                <input type="text" name="topic" id="topic" value="${epic.topic}" maxlength="32" placeholder="请输入主题"
                       title="主题">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">业务部门</td>
            <td>
                <input type="text" name="depat" id="depat" value="${epic.depat}" maxlength="32" placeholder="请输入业务部门"
                       title="业务部门">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">提出人</td>
            <td>
                <input type="text" name="upid" id="upid" value="${epic.upid}" maxlength="32" placeholder="请输入提出人"
                       title="提出人">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">优先级</td>
            <td>
                <select name="priority" id="priority">
                    <option value="1">重要</option>
                    <option value="2">紧急</option>
                    <option value="3">一般</option>
                </select>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">需求收集日期</td>
            <td>
                <input type="text" name="calldata" onClick="WdatePicker()" id="calldata" value="${epic.calldata}"
                       maxlength="32" placeholder="请输入受理日期" title="受理日期">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">需求分析开始时间</td>
            <td>
                <input type="text" name="begintime" onClick="WdatePicker()" id="begintime" value="${epic.begintime}"
                       maxlength="32" placeholder="请输入开始时间" title="开始时间">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">需求分析结束时间</td>
            <td>
                <input type="text" name="overtime" onClick="WdatePicker()" id="overtime" value="${epic.overtime}"
                       maxlength="32" placeholder="请输入结束时间" title="结束时间">
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">经办人</td>
            <td>
                <select name="vaiper" id="vaiper">
                    <c:forEach items="${userList}" var="user">
                        <option value="${user.account}">${user.name}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">描述</td>
            <td>
                <textarea name="remarks" id="remarks" rows="8" cols="50">${epic.remarks}</textarea>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">
                <input type="button" id="save" name="save" onclick="add()" value="保存">
            </td>
            <td>
                <input type="button" value="取消">
            </td>
        </tr>
    </table>
</form>
</body>
<script language="javascript" type="text/javascript" src="/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/common.js"></script>
<script type="text/javascript">
    /* 保存 */
    function add() {
        ///////////////////////////
        ///开始验证
        //////////////////////////

        if ($("#project").val() == "") {
            alert("项目名称不能为空，请重新输入！");
            return false;
        }
        if ($("#type").val() == "") {
            alert("问题类型不能为空，请重新输入！");
            return false;
        }
        if ($("#name").val() == "") {
            alert("史诗名称不能为空，请重新输入！");
            return false;
        }
        if ($("#topic").val() == "") {
            alert("主题不能为空，请重新输入！");
            return false;
        }
        if ($("#depat").val() == "") {
            alert("业务部门不能为空，请重新输入！");
            return false;
        }
        if ($("#upid").val() == "") {
            alert("提出人不能为空，请重新输入！");
            return false;
        }
        if ($("#priority").val() == "") {
            alert("优先级不能为空，请重新输入！");
            return false;
        }
        if ($("#calldata").val() == "") {
            alert("收集日期不能为空，请重新输入！");
            return false;
        }
        if ($("#begintime").val() == "") {
            alert("开始时间不能为空，请重新输入！");
            return false;
        }
        if ($("#overtime").val() == "") {
            alert("结束时间不能为空，请重新输入！");
            return false;
        }
        var endtime = $("#overtime").val();
        var starttime = $("#begintime").val();
        var start = new Date(starttime.replace("-", "/").replace("-", "/"));
        var end = new Date(endtime.replace("-", "/").replace("-", "/"));
        if (end < start) {
            alert('结束日期不能小于开始日期！');
            return false;
        }
        else {
            return true;
        }

        if ($("#viaper").val() == "") {
            alert("经办人不能为空，请重新输入！");
            return false;
        }
        ///////////////////////////
        ///验证结束
        //////////////////////////
        var epic = JSON.stringify($("#epicForm").serializeJson());
        var id = $("#epic_id").val();
        alert(id);
        if (id != "") {
            $.ajax({
                type: "post",
                url: "<%=basePath%>/epic/update/" + id,
                data: epic,
                contentType: "application/json",
                success: function (data) {
                    if (data == "success") {
                        alert("保存成功！");
                        window.location.reload();
                        <%--window.location.href = "<%=basePath%>/epic/list/1";--%>
                    } else {
                        alert(data);
                    }
                }
            });
        } else {
            $.ajax({
                type: "post",
                url: "<%=basePath%>epic/add",
                data: epic,
                contentType: "application/json",
                success: function (data) {
                    if (data == "success") {
                        alert("保存成功");
                        <%--window.location.href = "<%=basePath%>epic/list/1";--%>
                        window.location.reload();
                    } else {
                        alert(data);
                    }
                }
            })
        }

    }
</script>
</html>
