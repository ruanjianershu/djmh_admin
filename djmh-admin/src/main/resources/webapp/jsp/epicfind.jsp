<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/8
  Time: 11:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <tr>
        <td style="text-align:right;">项目:</td>
        <td>
            <input type="text" name="project" id="project" value="需求受理"/>
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">问题类型</td>
        <td>
            <input type="text" name="type" id="type" value="业务问题（史诗）"/>
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">Epic Name</td>
        <td>
            <input type="text" name="name" id="name" value="${epic.name}" maxlength="32" placeholder="请输入Epic Name"
                   title="Epic Name">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">主题</td>
        <td>
            <input type="text" name="topic" id="topic" value="${epic.topic}" maxlength="32" placeholder="请输入主题"
                   title="主题">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">业务部门</td>
        <td>
            <input type="text" name="depat" id="depat" value="${epic.depat}" maxlength="32" placeholder="请输入业务部门"
                   title="业务部门">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">提出人</td>
        <td>
            <input type="text" name="upid" id="upid" value="${epic.upid}" maxlength="32" placeholder="请输入提出人"
                   title="提出人">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">优先级</td>
        <td>
            <select name="priority" id="priority">
                <option value="1">重要</option>
                <option value="2">紧急</option>
                <option value="3">一般</option>
            </select>
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">需求收集日期</td>
        <td>
            <input type="text" name="calldata" id="calldata" value="${epic.calldata}" maxlength="32"
                   placeholder="请输入受理日期" title="受理日期">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">需求分析开始时间</td>
        <td>
            <input type="text" name="begintime" id="begintime" value="${epic.begintime}" maxlength="32"
                   placeholder="请输入开始时间" title="开始时间">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">需求分析结束时间</td>
        <td>
            <input type="text" name="overtime" id="overtime" value="${epic.overtime}" maxlength="32"
                   placeholder="请输入结束时间" title="结束时间">
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">经办人</td>
        <td>
            <select name="vaiper" id="vaiper">
                <c:forEach items="${userList}" var="user">
                    <option value="${user.account}">${user.name}</option>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">描述</td>
        <td>
            <textarea name="remarks" id="remarks" rows="8" cols="50">${epic.remarks}</textarea>
        </td>
    </tr>
</table>
</body>
<script type="text/javascript" src="/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    /* 关闭 */
    function close() {
        window.location.href = "<%=basePath%>user/list";
    }
</script>
</html>
