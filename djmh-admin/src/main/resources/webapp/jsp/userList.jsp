<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8"
        pageEncoding="UTF-8" %>
<taglib uri="http://java.sun.com/jstl/core" prefix="c"/>
<!DOCTYPEhtml PUBLIC "-//W3C//DTDHTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<head>
    <title>用户列表</title>
</head>

<form action="/user/findAllByTerm" id="selAll" method="post">
    <input type="submit" id="sel" value="查询员工"/>
    登录账号：<input type="text" name="account" id="account" maxlength="20"><%--<i>账号不能超过20个字符,唯一性</i>--%>
    姓名:<input type="text" name="name" id="name" maxlength="20">
    基础角色：<select name="role" id="role">
    <option value="">请选择</option>
    <option value="1">产品经理</option>
</select>
</form>
<input id="save" type="button" value="添加员工" onclick="save()">

<form method="post" id="userForm">
    <table border="1 px solid">
        <tr>
            <th>用户ID</th>
            <th>用户账号</th>
            <th>用户姓名</th>
            <th>中文全拼</th>
            <th>基础角色</th>
            <th>状态</th>
            <th>生日</th>
            <th>手机号</th>
            <th>操作</th>
        </tr>
        <c:forEach var="user" items="${pageInfo.list}">
            <tr>
                <td>${user.id}</td>
                <td>${user.account}</td>
                <td>${user.name}</td>
                <td>${user.pinyin}</td>
                <td>${user.role}</td>
                <td>${user.state}</td>
                <td>${user.birthday}</td>
                <td>${user.phone}</td>
                <td>
                    <input id="edit" type="button" value="编辑" onclick="update('${user.account}');">
                    &nbsp;|&nbsp;
                    <input id="checkAll" type="button" value="查看" onclick="findById('${user.account}');">
                </td>
            </tr>
        </c:forEach>
    </table>
    <div class="pagin">
        <div class="message">
            共<i class="blue">${pageInfo.total }</i>条记录，当前显示第&nbsp;<i
                class="blue">${pageInfo.pageNum }&nbsp;/&nbsp;${pageInfo.pages }</i>页
        </div>

        <ul class="paginList">
            <li class="paginItem"><a href="javascript:;" id="begin">首页</a></li>
            <li class="paginItem"><a href="javascript:;" id="pre">上一页</a></li>
            <li class="paginItem"><a href="javascript:;" id="next">下一页</a></li>
            <li class="paginItem"><a href="javascript:;" id="end">尾页</a></li>
        </ul>
    </div>
    <input type="hidden" name="pageNow" value="${pageInfo.pageNum }" id="pageNow">
    <input type="hidden" name="totalPages" value="${pageInfo.pages }" id="totalPages">
</form>
</body>
<script type="text/javascript" src="/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/layer/layer.js"></script>
<script type="text/javascript">
    /* 跳转到修改页面 */
    function update(account) {
        parent.layer.open({
            type: 2,
            title: "人员管理 | 更新",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>user/goUpdate/' + account,
            end: function () {
                location.reload();
            }

        });
        <%--window.location.href = "<%=basePath%>user/goUpdate/"+account;--%>
    }

    /* 跳转到查看页面 */
    function findById(account) {
        parent.layer.open({
            type: 2,
            title: "人员管理 | 查看",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>user/goFind/' + account,
            end: function () {
                location.reload();
            }

        });
        <%--window.location.href = "<%=basePath%>user/goFind/"+account;--%>
    }

    /* 跳转到新增页面 */
    function save() {
        parent.layer.open({
            type: 2,
            title: "人员管理 | 添加",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>user/goAdd',
            end: function () {
                location.reload();
            }

        });
        <%--window.location.href = "<%=basePath%>user/goAdd";--%>
    }
</script>

<script type="text/javascript">
    //分页操作
    $(function () {
        $("#begin").on("click", function () {
            var pageNow = $("#pageNow").val(1);
            //请求
            document.getElementById("userForm").action = "/user/list/" + 1
            document.getElementById("userForm").submit();
        });

        $("#pre").on("click", function () {
            var pagerNow = $("#pageNow").val();
            if (pagerNow == 1) {
                return false;
            }
            $("#pageNow").val(parseInt(pagerNow) - 1);
            pagerNow = $("#pageNow").val();
            //请求
            document.getElementById("userForm").action = "/user/list/" + pagerNow
            document.getElementById("userForm").submit();
        });

        $("#next").on("click", function () {
            var pagerNow = $("#pageNow").val();
            var totalPages = $("#totalPages").val();
            if (pagerNow == totalPages) {
                return false;
            } else {
                $("#pageNow").val(parseInt(pagerNow) + 1);
                //请求
                pagerNow = $("#pageNow").val();
                document.getElementById("userForm").action = "/user/list/" + pagerNow
                document.getElementById("userForm").submit();
            }

        });

        $("#end").click(function () {
            console.log('123');
            var totalPages = $("#totalPages").val();
            $("#pageNow").val(totalPages);
            var pagerNow = $("#pageNow").val();
            //请求
            document.getElementById("userForm").action = "/user/list/" + pagerNow
            document.getElementById("userForm").submit();
        });
    })
</script>
</html>