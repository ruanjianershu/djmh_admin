<%--
  Created by IntelliJ IDEA.
  User: 11415
  Date: 2017/11/8
  Time: 10:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<head>
    <title>welcome</title>
</head>
<body>
<form id="login">
    <td>
        <tr>请输入账号:</tr>
        <tr>
            <input type="text" name="account" id="account" placeholder="请输入账号"/>
        </tr>
    </td>
    <tr>
        <td>请输入密码:</td>
        <td>
            <input type="password" name="pswd" id="pswd" placeholder="请输入密码"/>
        </td>
    </tr>
    <tr>
        <td class="login_info">验证码：</td>
        <td class="width70"><input id="checkCode" name="checkCode" type="text" class="width70"/></td>
        <td><img src="createImage" alt="验证码" title="点击更换" onclick="this.src='createImage?'+(new Date()).getTime();"/>
        </td>
        <td><span id="checkCodeMsg" class="required"></span></td>
    </tr>
    <input type="button" id="loginbtn" onclick="login()" value="登录">
    <input type="button" name="pass" onclick="forget()" value="重置密码">
    <input type="button" name="pass" onclick="password()" value="修改密码">
</form>
</body>
<script type="text/javascript" src="<%=basePath%>/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/layer/layer.js"></script>
<script type="text/javascript" src="/common.js"></script>
<script type="text/javascript">
    function login() {
        var id1 = document.getElementById("account");
        var pass = document.getElementById("pswd");
        var checkCode = document.getElementById("checkCode");
        var cheek = checkCode.value;
        if (cheek == "") {
            alert("验证码不能为空");
            return false;
        }
        var account = id1.value;
        if (account == "") {
            alert("账号不能为空");
            return false;
        }
        var psw = pass.value;
        if (psw == "") {
            alert("密码不能为空");
            return false;
        } else {
            alert(cheek);
            $.ajax({
                type: "POST",
                datatype: "JSON",
                url: "<%=basePath%>user/login/" + account + "/" + psw + "/" + cheek,
                success: function (data) {
                    if (data === "success") {
                        window.location.href = "goSys"
                    } else {
                        alert(data);
                        return false;
                    }

                }
            })
        }

    }

    /* 跳转到修改页面 */
    function password() {
        parent.layer.open({
            type: 2,
            title: "人员管理 | 修改密码",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>user/goPassword/'

        });
    }

    /* 跳转到修改页面 */
    function forget() {
        var id1 = document.getElementById("account");
        var account = id1.value;
        alert(account);
        if (account == "") {
            alert("请输入你要重置的账号");
        } else {
            $.ajax({
                type: "POST",
                datatype: "JSON",
                url: "<%=basePath%>user/forget/" + account,
                success: function (data) {
                    if (data === "success") {
                        alert("重置成功,请登录");
                        return false;
                    } else {
                        alert("账号错误，请重新操作!");
                        return false;
                    }

                }
            })
        }

    }

</script>
</html>