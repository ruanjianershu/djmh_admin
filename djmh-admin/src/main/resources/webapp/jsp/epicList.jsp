<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8"
        pageEncoding="UTF-8" %>
<taglib uri="http://java.sun.com/jstl/core" prefix="c"/>
<!DOCTYPEhtml PUBLIC "-//W3C//DTDHTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<head>
    <title>史诗管理</title>
</head>

<form action="/epic/findAllByTerm" id="selAll" method="post">
    <input type="submit" id="sel" value="查询"/>
    史诗编号：<input type="text" name="id" id="id" maxlength="20"><%--<i>账号不能超过20个字符,唯一性</i>--%>
    提出人:<input type="text" name="upid" id="upid" maxlength="20">
    主题:<input type="text" name="topic" id="topic" maxlength="20">
    状态：<select name="status" id="status">
    <option value="">请选择</option>
    <option value="1">关闭</option>
    <option value="2">完成</option>
    <option value="3">进行中</option>
</select>
</form>
<input id="save" type="button" value="添加新史诗" onclick="save()">
<form method="post" id="epicForm">
    <table border="1 px solid" style="p">
        <tr>
            <th>业务部门</th>
            <th>提出人</th>
            <th>史诗编号</th>
            <th>项目名称</th>
            <th>问题类型</th>
            <th>主题</th>
            <th>状态</th>
            <th>受理日期</th>
            <th>分析开始时间</th>
            <th>分析完成时间</th>
            <th>操作</th>
        </tr>
        <c:forEach var="emp" items="${pageInfo.list}">
            <tr>
                <td>${emp.depat}</td>
                <td>${emp.upid}</td>
                <td>${emp.id}</td>
                <td>${emp.project}</td>
                <td>${emp.type}</td>
                <td>${emp.topic}</td>
                <td>${emp.status}</td>
                <td>${emp.calldata}</td>
                <td>${emp.begintime}</td>
                <td>${emp.overtime}</td>
                <td>
                    <input id="edit" type="button" value="编辑" onclick="update('${emp.id}');">
                    &nbsp;|&nbsp;
                    <input id="checkAll" type="button" value="查看" onclick="findById('${emp.id}');">
                </td>
            </tr>
        </c:forEach>

    </table>
    <div class="pagin">
        <div class="message">
            共<i class="blue">${pageInfo.total }</i>条记录，当前显示第&nbsp;<i
                class="blue">${pageInfo.pageNum }&nbsp;/&nbsp;${pageInfo.pages }</i>页
        </div>

        <ul class="paginList">
            <li class="paginItem"><a href="javascript:;" id="begin">首页</a></li>
            <li class="paginItem"><a href="javascript:;" id="pre">上一页</a></li>
            <li class="paginItem"><a href="javascript:;" id="next">下一页</a></li>
            <li class="paginItem"><a href="javascript:;" id="end">尾页</a></li>
        </ul>
    </div>
    <input type="hidden" name="pageNow" value="${pageInfo.pageNum }" id="pageNow">
    <input type="hidden" name="totalPages" value="${pageInfo.pages }" id="totalPages">
</form>
</body>
<script type="text/javascript" src="/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/layer/layer.js"></script>

<script type="text/javascript">
    //分页操作
    $(function () {
        $("#begin").on("click", function () {
            var pageNow = $("#pageNow").val(1);
            //请求
            document.getElementById("epicForm").action = "/epic/list/" + 1
            document.getElementById("epicForm").submit();
        });

        $("#pre").on("click", function () {
            var pagerNow = $("#pageNow").val();
            if (pagerNow == 1) {
                return false;
            }
            $("#pageNow").val(parseInt(pagerNow) - 1);
            pagerNow = $("#pageNow").val();
            //请求
            document.getElementById("epicForm").action = "/epic/list/" + pagerNow
            document.getElementById("epicForm").submit();
        });

        $("#next").on("click", function () {
            var pagerNow = $("#pageNow").val();
            var totalPages = $("#totalPages").val();
            if (pagerNow == totalPages) {
                return false;
            } else {
                $("#pageNow").val(parseInt(pagerNow) + 1);
                //请求
                pagerNow = $("#pageNow").val();
                document.getElementById("epicForm").action = "/epic/list/" + pagerNow
                document.getElementById("epicForm").submit();
            }

        });

        $("#end").click(function () {
            console.log('123');
            var totalPages = $("#totalPages").val();
            $("#pageNow").val(totalPages);
            var pagerNow = $("#pageNow").val();
            //请求
            document.getElementById("epicForm").action = "/epic/list/" + pagerNow
            document.getElementById("epicForm").submit();
        });
    })
</script>
<script type="text/javascript">
    /* 跳转到修改页面 */
    function update(id) {
        parent.layer.open({
            type: 2,
            title: "史诗管理 | 更新",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: ['<%=basePath%>epic/goUpdate/' + id, 'yes'],
            end: function () {
                location.reload();
            }
        });
    }

    /* 跳转到查看页面 */
    function findById(id) {
        parent.layer.open({
            type: 2,
            title: "史诗管理 | 查看",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>epic/goFind/' + id,
            end: function () {
                location.reload();
            }

        });
    }

    /* 跳转到新增页面 */
    function save() {
        parent.layer.open({
            type: 2,
            title: "史诗管理 | 添加",
            area: ['700px', '530px'],
            fixed: false, //不固定
            //maxmin: true,
            content: '<%=basePath%>epic/goAdd',
            end: function () {
                location.reload();
            }
        });
    }
</script>
</html>