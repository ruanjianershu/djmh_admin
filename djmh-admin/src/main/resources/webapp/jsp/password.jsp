<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    //项目的发布路径，例如:  /rabc
    String path = request.getContextPath();
	/*
	全路径，形式如下: http://127.0.0.1:8001/rbac/
	request.getScheme()      ——> http 获取协议
	request.getServerName()  --> 127.0.0.1 获取服务名
	request.getServerPort()  --> 8001 获取端口号
	path                     --> /rbac 获取访问的路径 路
	*/
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<%-- 
	<base/>标签解决路径问题
	参考文章:http://www.cnblogs.com/muqianying/archive/2012/03/16/2400280.html
--%>
<!DOCTYPE HTML>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>部门编辑界面</title>
</head>

<body>
<div class="formbody">

    <div class="formtitle">
        <span>密码更改页面</span>
    </div>
    <form action="user/password" method="post" id="userForm">
        <ul class="forminfo">
            <li><label>请输入登录账号</label><input name="account" id="account" value=""
                                             type="text" class="dfinput" maxlength="20" autofocus="autofocus"/><i
                    id="error"></i></li>
            <li><label>请输入原密码</label><input name="o_password" id="o_password" value=""
                                            type="text" class="dfinput" maxlength="20" autofocus="autofocus"/><i
                    id="error"></i></li>
            <li><label>请输入新密码</label><input name="new_password" id="new_password" value=""
                                            type="text" class="dfinput" maxlength="20" autofocus="autofocus"/><i
                    id="error"></i></li>
            <li><label>请输入新密码</label><input name="password" id="password" value=""
                                            type="text" class="dfinput" maxlength="20" autofocus="autofocus"/><i
                    id="error"></i></li>
            <tr>
                <td class="login_info">验证码：</td>
                <td class="width70"><input id="checkcode" name="checkcode" type="text" class="width70"/></td>
                <td><img src="createImage" alt="验证码" title="点击更换"
                         onclick="this.src='createImage?'+(new Date()).getTime();"/></td>
                <td><span id="checkcode_msg" class="required"></span></td>
            </tr>
            <li><label>&nbsp;</label><input name="" type="submit"
                                            class="btn" value="确认更新"/></li>
        </ul>
    </form>
</div>
<script type="text/javascript" src="/jquery-1.11.3.js"></script>
<script type="text/javascript">
    $(function () {
        var count = 0;
        $(".btn").on("click", function () {
            console.log("123");
            if (count < 2) {
                var o_password = $.trim($("#o_password").val());
                if (o_password === "") {
                    alert("密码不能为空");
                    $("#old_password").focus();
                    return false;
                }
                var account = $.trim($("#account").val());
                if (account === "") {
                    alert("账号不能为空！");
                    $("#account").focus();
                    count++;
                    return false;
                } else {
                    var new_password = $.trim($("#new_password").val());
                    var password = $.trim($("#password").val());
                    if (new_password === "") {
                        alert("密码不能为空");
                        $("#new_password").focus();
                        count++;
                        return false;
                    }
                    if (new_password != password) {
                        alert("两次密码不一致，请重新输入！");
                        $("#new_password").focus();
                        count++;
                        return false;
                    } else {
                        document.getElementById("userForm").action = "/user/password/" + o_password
                        //修改成功，重新登录
//	                      window.top.location.href="/wel";
                    }
                }
            } else {
                //重新登录
                window.top.location.href = "/wel";
                return;
            }
        });
    });
</script>
</body>
</html>