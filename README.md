
## 吉林建筑大学电气与计算机学院门户后台

### 项目框架由SpringBoot + MyBatis + Spring 搭建

### 由gradle提供jar包的控制

### 项目由swagger提供后台接口测试；后台接口访问：http://localhost:8080/swagger-ui.html

### 服务部署时注意gradle 安装及缓存问题，部署脚本在项目的sh文件夹

### 项目地址：https://gitee.com/ruanjianershu/djmh_admin



#### 欢迎各位交流学习 ： liting08011@163.com